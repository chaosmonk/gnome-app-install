# Romanian translation for trisquel-app-install
# Copyright (c) (c) 2005 Canonical Ltd, and Rosetta Contributors 2005
# This file is distributed under the same license as the trisquel-app-install package.
# Dan Damian <dand@ubuntu.ro>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: trisquel-app-install\n"
"Report-Msgid-Bugs-To: Sebastian Heinlein <sebi@glatzor.de>\n"
"POT-Creation-Date: 2009-08-10 19:10+0200\n"
"PO-Revision-Date: 2009-07-28 14:14+0000\n"
"Last-Translator: Dan Damian <dand@ubuntu.ro>\n"
"Language-Team: Romanian <ubuntu-ro@lists.ubuntu.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-08-10 16:00+0000\n"
"X-Generator: Launchpad (build Unknown)\n"
"X-Rosetta-Version: 0.1\n"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:1
msgid "Components that have been confirmed to install applications from"
msgstr "Componentele ce au fost confirmate pentru a instala aplicații de la"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:2
msgid "If true, the main window is maximized."
msgstr "Dacă fereastră prinicpală este maximizată"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:3
msgid "Main window height"
msgstr "Înalțimea ferestrei principale"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:4
msgid "Main window maximization state"
msgstr "Stare de maximizarea a ferestrei principale"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:5
msgid "Main window width"
msgstr "Lățimea ferestrei principale"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:6
msgid ""
"Possible values: 0 : show all available applications 1 : show only free "
"applications 2 : - 3 : show only supported applications 4 : show only 3rd "
"party applications 5 : - 6 : show only installed applications"
msgstr ""
"Valori posibile 0 : afișează toate aplicațiile 1 : afișează doar aplicațiile "
"libere 2 : - 3 : afișează doar aplicațiile suportate 4 : afișează doar "
"aplicațiile de la surse terțe 5 : - 6 : afișează doar aplicațiile instalate"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:7
msgid "Show only a subset of applications"
msgstr "Afișeasă doar un subset al aplicațiilor"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:8
msgid "Suggest application for MIME-type: approved components"
msgstr "Sugerați apliacția pentru tipul MIME: compenente aprobate"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:9
msgid "Timeout for interactive search"
msgstr "Întârzierea pentru căutarea interactivă"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:10
msgid ""
"Timeout from typing in the search entry until a search is triggered (in "
"milliseconds)."
msgstr ""
"Durata de la oprirea din tastarea după care se pornește căutarea automată "
"(în milisecunde)."

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:11
msgid "Timestamp of the last cache refresh run"
msgstr "Data ultimei reîmprospătări a informațiilor stocate"

#: ../data/org.trisquel.trisquel-app-install.gschema.xml.in.h:12
msgid ""
"When the user asks to open a file but no supported application is present, "
"the system will offer to install applications in the components listed here, "
"as well as the individually whitelisted applications."
msgstr ""
"În momentul în care utilizatorul încearcă să deschidă un fișier pentru care "
"nu există instalată o aplicație corespunzătoare, sistemul va întreba "
"utilizatorul dacă dorește să instaleze aplicații din componentele de aici, "
"precum și dintre aplicațiile activate individual."

#: ../data/trisquel-app-install.ui.h:1
msgid "<b>Add</b>"
msgstr "<big>Instalează</big>"

#: ../data/trisquel-app-install.ui.h:2
msgid "<b>Remove</b>"
msgstr "<big>Dezinstalează</big>"

#: ../data/trisquel-app-install.ui.h:6
#, fuzzy
msgid ""
"<big><b>The list of available applications is out of date</b></big>\n"
"\n"
"To reload the list you need a working internet connection."
msgstr ""
"Clic pe „Reîncarcă” pentru a o încărca. Aveți nevoie de o conexiune la "
"Internet pentr a reîncărca lista. "

#: ../data/trisquel-app-install.ui.h:9
#, fuzzy
msgid "Add/Remove Applications"
msgstr "Aplicații"

#: ../data/trisquel-app-install.ui.h:10
msgid "Apply pending changes and close the window"
msgstr ""

#: ../data/trisquel-app-install.ui.h:11
msgid "Cancel pending changes and close the window"
msgstr ""

#: ../data/trisquel-app-install.ui.h:12
msgid "Categories"
msgstr "Categorii"

#: ../data/trisquel-app-install.ui.h:13
msgid "Search:"
msgstr "Caută:"

#: ../data/trisquel-app-install.ui.h:14
msgid "Show:"
msgstr ""

#: ../data/trisquel-app-install.ui.h:15
msgid "The categories represent the application menu"
msgstr ""

#: ../data/trisquel-app-install.ui.h:16
#, fuzzy
msgid "_Add/Remove More Applications"
msgstr "Instalează/Dezinstalaeză mai multe programe"

#: ../data/trisquel-app-install.ui.h:17
msgid "_Apply Changes"
msgstr "_Aplică schimbările"

#: ../data/trisquel-app-install.ui.h:18
msgid "_Reload"
msgstr "_Reîncarcă"

#: ../data/trisquel-app-install.ui.h:19
msgid "_Remove"
msgstr "_Dezinstalează"

#: ../data/trisquel-app-install.ui.h:20
msgid "_Retry"
msgstr "_Reîncearcă"

#: ../data/trisquel-app-install.ui.h:21
#, fuzzy
msgid "applications"
msgstr "Aplicații"

#: ../data/trisquel-app-install.desktop.in.h:1
#: ../data/trisquel-app-install-xfce.desktop.in.h:1
msgid "Add/Remove..."
msgstr "Instalare/Dezinstalare..."

#: ../data/trisquel-app-install.desktop.in.h:2
#: ../data/trisquel-app-install-xfce.desktop.in.h:2
msgid "Install and remove applications"
msgstr "Instalare și dezinstalare de aplicații"

#: ../data/trisquel-app-install.desktop.in.h:3
#: ../data/trisquel-app-install-xfce.desktop.in.h:3
msgid "Package Manager"
msgstr "Administrator de pachete"

#: ../AppInstall/activation.py:124
msgid "no suitable application"
msgstr "nu există aplicații corespunzătoare"

#: ../AppInstall/activation.py:125
msgid ""
"No application suitable for automatic installation is available for handling "
"this kind of file."
msgstr ""
"Pentru deschiderea acestui tip de fișiere nu există nici o aplicație care să "
"poată fi instalat automat."

#: ../AppInstall/activation.py:128
msgid "no application found"
msgstr "nu s-au găsit aplicații"

#: ../AppInstall/activation.py:129
msgid "No application is known for this kind of file."
msgstr "Pentru acest tip de fișier nu se cunoaște nici o aplicație"

#: ../AppInstall/activation.py:142 ../AppInstall/activation.py:336
msgid "Searching for appropriate applications"
msgstr "Se caută aplicații corespunzătoare"

#: ../AppInstall/activation.py:144 ../AppInstall/activation.py:217
msgid "Please wait. This might take a minute or two."
msgstr ""
"Vă rugăm așteptați. Această operație poate să dureze un minut sau două."

#: ../AppInstall/activation.py:172
msgid "Search for suitable codec?"
msgstr "Caută pentru codecuri corespunzătoare?"

#: ../AppInstall/activation.py:173
msgid ""
"The required software to play this file is not installed. You need to "
"install suitable codecs to play media files. Do you want to search for a "
"codec that supports the selected file?\n"
"\n"
"The search will also include software which is not officially supported."
msgstr ""
"Nu este instalată nici o aplicație ce poate reda acest fișier. Pentru a reda "
"fișiere multimedia trebuie să instalați codecuri corespunzatoare. Doriți "
"efectuarea unei căutări pentru codecuri corespunzătoare?\n"
"\n"
"Căutarea va include și programe ce nu sunt suportate oficial."

#: ../AppInstall/activation.py:184
msgid "Invalid commandline"
msgstr "Linia de comandă nu este validă"

#: ../AppInstall/activation.py:185
#, python-format
msgid "'%s' does not understand the commandline argument '%s'"
msgstr "'%s' nu acceptă argumentul '%s' din linia de comandă"

#: ../AppInstall/activation.py:215
msgid "Searching for appropriate codecs"
msgstr "Se caută codecurile corespunzătoare"

#: ../AppInstall/activation.py:220 ../AppInstall/activation.py:341
msgid "_Install"
msgstr "_Instalează"

#: ../AppInstall/activation.py:221
msgid "Install Media Plug-ins"
msgstr "Instalare module media"

#: ../AppInstall/activation.py:225
msgid "Codec"
msgstr "Codec"

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:303
#, python-format
msgid "\"%s\" cannot be opened"
msgstr "nu s-a putut deschide „%s”"

#: ../AppInstall/activation.py:338
#, python-format
msgid ""
"A list of applications that can handle documents of the type '%s' will be "
"created"
msgstr "Se va creea o listă cu aplicații ce pot lucra cu fișiere de tipul „%s”"

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:344
#, python-format
msgid "Install applications to open \"%s\""
msgstr "Instalează aplicații pentru a deschide „%s”"

#: ../AppInstall/activation.py:347
msgid "Install applications"
msgstr "Instalare aplicații"

#: ../AppInstall/activation.py:358
msgid "_Search"
msgstr "_Caută"

#: ../AppInstall/activation.py:397
msgid "Searching for extensions"
msgstr "Căutare extensii"

#: ../AppInstall/activation.py:398
msgid "Extensions allow you to add new features to your application."
msgstr "Extensiile vă permit să adăugați aplicațiilor funcționalități noi."

#: ../AppInstall/activation.py:400
msgid "Install/Remove Extensions"
msgstr "Instalează/Dezinstalează extensii"

#: ../AppInstall/activation.py:402
msgid "Extension"
msgstr "Extensie"

#: ../AppInstall/AppInstallApp.py:177
msgid ""
"To install an application check the box next to the application. Uncheck the "
"box to remove the application."
msgstr ""
"Pentru a instala o aplicație, bifați căsuța din dreptul ei. Debifați căsuța "
"pentru a dezinstala aplicația."

#: ../AppInstall/AppInstallApp.py:180
msgid "To perform advanced tasks use the Synaptic package manager."
msgstr ""
"Folosiți Administratorul de pachete Synaptic pentru a efectua sarcini "
"avansate."

#: ../AppInstall/AppInstallApp.py:182
msgid "Quick Introduction"
msgstr "Scurtă introducere"

#: ../AppInstall/AppInstallApp.py:228
msgid "Installed applications only"
msgstr "Doar aplicații instalate"

#: ../AppInstall/AppInstallApp.py:229
msgid "Show only applications that are installed on your computer"
msgstr "Afișează doar aplicațiile instalate pe calculatorul dumneavoastră"

#: ../AppInstall/AppInstallApp.py:351
msgid "Error reading the addon CD"
msgstr "Eroare la citirea CD-ului"

#: ../AppInstall/AppInstallApp.py:352
msgid "The addon CD may be corrupt "
msgstr "CD-ul poate fi deteriorat "

#: ../AppInstall/AppInstallApp.py:440
msgid "The list of applications is not available"
msgstr "Lista aplicațiilor nu este disponibilă"

#: ../AppInstall/AppInstallApp.py:441
msgid ""
"Click on 'Reload' to load it. To reload the list you need a working internet "
"connection. "
msgstr ""
"Clic pe „Reîncarcă” pentru a o încărca. Aveți nevoie de o conexiune la "
"Internet pentr a reîncărca lista. "

#: ../AppInstall/AppInstallApp.py:460
#, python-format
msgid "%s cannot be installed on your computer type (%s)"
msgstr "%s nu poate fi instalat pe tipul calculatorului dumneavoastră (%s)"

#: ../AppInstall/AppInstallApp.py:463
msgid ""
"Either the application requires special hardware features or the vendor "
"decided to not support your computer type."
msgstr ""
"Fie că aplicația necesită caracteristici speciale pentru hardware sau "
"furnizorul nu suportă tipul calculatorului dumneavoastră."

#: ../AppInstall/AppInstallApp.py:489
#, python-format
msgid "Cannot install '%s'"
msgstr "Nu s-a putut instala „%s”"

#: ../AppInstall/AppInstallApp.py:490
#, python-format
msgid ""
"This application conflicts with other installed software. To install '%s' "
"the conflicting software must be removed first.\n"
"\n"
"Switch to the 'synaptic' package manager to resolve this conflict."
msgstr ""
"Această aplicație intră în conflict cu alte programe instalate. Pentru a "
"instala „%s” trebuie dezinstalat programul cu care intră în conflict.\n"
"\n"
"Pentru a rezolva acest conflict folosiți Administratorul de pachete Synaptic."

#: ../AppInstall/AppInstallApp.py:542
#, python-format
msgid "Cannot remove '%s'"
msgstr "Nu s-a putut dezinstala „%s”"

#: ../AppInstall/AppInstallApp.py:543
#, python-format
msgid ""
"One or more applications depend on %s. To remove %s and the dependent "
"applications, use the Synaptic package manager."
msgstr ""
"Una sau mai multe aplicații depind de %s. Folosiți Administratorul de "
"pachete Synaptic pentru a dezinstala %s împreună cu aplicațiile dependente."

#: ../AppInstall/AppInstallApp.py:639
msgid "C_onfirm"
msgstr "C_onfirmare"

#. Fallback
#: ../AppInstall/AppInstallApp.py:680 ../AppInstall/DialogProprietary.py:22
#: ../AppInstall/distros/Debian.py:24
#, python-format
msgid "Enable the installation of software from %s?"
msgstr "Activează instalarea programelor de la %s?"

#: ../AppInstall/AppInstallApp.py:682
#, python-format
msgid ""
"%s is provided by a third party vendor. The third party vendor is "
"responsible for support and security updates."
msgstr ""
"%s este oferit de o terță sursă. Sursa terță este responsabilă pentru "
"suportul aplicației și oferirea actualizărilor de securitate."

#: ../AppInstall/AppInstallApp.py:686 ../AppInstall/DialogProprietary.py:25
msgid "You need a working internet connection to continue."
msgstr "Pentru a continua aveți nevoie de o conexiune la Internet."

#: ../AppInstall/AppInstallApp.py:690 ../AppInstall/DialogProprietary.py:35
msgid "_Enable"
msgstr "Activ_ează"

#. show an error dialog if something went wrong with the cache
#: ../AppInstall/AppInstallApp.py:899
msgid "Failed to check for installed and available applications"
msgstr "Verificare pentru aplicațiile instalate sau disponibile a eșuat"

#: ../AppInstall/AppInstallApp.py:900
msgid ""
"This is a major failure of your software management system. Please check for "
"broken packages with synaptic, check the file permissions and correctness of "
"the file '/etc/apt/sources.list' and reload the software information with: "
"'sudo apt-get update' and 'sudo apt-get install -f'."
msgstr ""
"Aceasta este o eroare majoră a sistemului de administrare a softwareului. "
"Verificați pachetele deterioarate folosind synaptic, verificați permisiunile "
"fișierelor și corectitudinea fișierului '/etc/apt/sources.list' și "
"reîncărcați informațiile software folosind 'sudo apt-get update' și 'sudo "
"apt-get install -f'."

#: ../AppInstall/AppInstallApp.py:978
msgid "Apply changes to installed applications before closing?"
msgstr ""
"Înainte de a închide aplică modificările asupra aplicațiilor instalate?"

#: ../AppInstall/AppInstallApp.py:979
msgid "If you do not apply your changes they will be lost permanently."
msgstr ""
"În cazul în care nu aplicați modificările, ele vor fi pierdute definitiv."

#: ../AppInstall/AppInstallApp.py:983
msgid "_Close Without Applying"
msgstr "În_chide fără a aplica modificările"

#: ../AppInstall/AppInstallApp.py:1000
msgid "No help available"
msgstr "Nici un ajutor disponibil"

#: ../AppInstall/AppInstallApp.py:1001
msgid "To display the help, you need to install the \"yelp\" application."
msgstr "Pentru a afișa ajutorul, trebuie să instalați aplicația „yelp”."

#. FIXME: move this inside the dialog class, we show a different
#. text for a quit dialog and a approve dialog
#: ../AppInstall/AppInstallApp.py:1050
msgid "Apply the following changes?"
msgstr "Doriți să aplicați următoarele schimbări?"

#: ../AppInstall/AppInstallApp.py:1051
msgid ""
"Please take a final look through the list of applications that will be "
"installed or removed."
msgstr ""
"Revizuiți lista de aplicații ce urmează a fi instalate sau dezinstalate."

#: ../AppInstall/AppInstallApp.py:1272
msgid "There is no matching application available."
msgstr "Nu s-a găsit nici o aplicație."

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstallApp.py:1282
#, python-format
msgid "To broaden your search, choose \"%s\"."
msgstr "Alegeți „%s” pentru a mări domeniul de căutare."

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstallApp.py:1286
#, python-format
msgid "To broaden your search, choose \"%s\" or \"%s\"."
msgstr "Alegeți „%s” sau „%s” pentru a mări domeniul de căutare."

#. TRANSLATORS: Show refers to the Show: combobox
#: ../AppInstall/AppInstallApp.py:1292
msgid "To broaden your search, choose a different \"Show\" item."
msgstr ""
"Pentru a mări domeniul de căutare alegeți o altă intrare din meniul "
"„Afișează”"

#. TRANSLATORS: All refers to the All category in the left list
#: ../AppInstall/AppInstallApp.py:1298
msgid "To broaden your search, choose 'All' categories."
msgstr "Pentru a mări domeniul de căutare alegeți categoria „Toate”"

#: ../AppInstall/BrowserView.py:96
#, python-format
msgid "Failed to open '%s'"
msgstr "Nu s-a putut deschide „%s”"

#: ../AppInstall/CoreMenu.py:195
msgid "Applications"
msgstr "Aplicații"

#: ../AppInstall/DialogComplete.py:130
msgid "Software installation failed"
msgstr "Instalarea programului a eșuat"

#: ../AppInstall/DialogComplete.py:131
msgid ""
"There has been a problem during the installation of the following pieces of "
"software."
msgstr "În timpul instalării următoarelor componente a apărut o eroare."

#: ../AppInstall/DialogComplete.py:133 ../AppInstall/DialogComplete.py:149
#: ../AppInstall/DialogComplete.py:165 ../AppInstall/DialogComplete.py:193
msgid "Add/Remove More Software"
msgstr "Instalează/Dezinstalaeză mai multe programe"

#: ../AppInstall/DialogComplete.py:135
msgid "Application installation failed"
msgstr "Instalarea aplicației a eșuat"

#: ../AppInstall/DialogComplete.py:136
msgid ""
"There has been a problem during the installation of the following "
"applications."
msgstr "În timpul instalării următoarelor programe a apărut o eroare."

#: ../AppInstall/DialogComplete.py:146
msgid "Software could not be removed"
msgstr "Nu s-a putut dezinstala programul"

#: ../AppInstall/DialogComplete.py:147
msgid ""
"There has been a problem during the removal of the following pieces of "
"software."
msgstr ""
"În timpul dezinstalării următoarelor componente de programe a apărut o "
"eroare."

#: ../AppInstall/DialogComplete.py:151
msgid "Not all applications could be removed"
msgstr "Nu toate aplicațiile au putut fi dezinstalate"

#: ../AppInstall/DialogComplete.py:152
msgid ""
"There has been a problem during the removal of the following applications."
msgstr "În timpul instalării următoarelor programe a apărut o eroare."

#: ../AppInstall/DialogComplete.py:162
msgid "Installation and removal of software failed"
msgstr "Instalarea și dezinstalarea programului a eșuat"

#: ../AppInstall/DialogComplete.py:163
msgid ""
"There has been a problem during the installation or removal of the following "
"pieces of software."
msgstr ""
"În timpul instalării sau dezinstalării următoarelor componente de programe a "
"apărut o eroare."

#: ../AppInstall/DialogComplete.py:167
msgid "Installation and removal of applications failed"
msgstr "Instalarea și dezinstalarea aplicațiilor a eșuat"

#: ../AppInstall/DialogComplete.py:168
msgid ""
"There has been a problem during the installation or removal of the following "
"applications."
msgstr ""
"În timpul instalării sau dezinstalării următoarelor programe a apărut o "
"eroare."

#: ../AppInstall/DialogComplete.py:176
msgid "New application has been installed"
msgstr "Noua aplicație a fost instalată"

#: ../AppInstall/DialogComplete.py:177
msgid "New applications have been installed"
msgstr "Noile aplicații au fost instalate"

#: ../AppInstall/DialogComplete.py:182
msgid ""
"To start a newly installed application, choose it from the applications menu."
msgstr "Pentru a porni o aplicație nou instalată, alege aplicația din meniu."

#: ../AppInstall/DialogComplete.py:185
msgid "To start a newly installed application double click on it."
msgstr "Pentru a porni o aplicație nou instalată, faceți dublu clic pe ea."

#: ../AppInstall/DialogComplete.py:189
msgid "Software has been installed successfully"
msgstr "Programul a fost instalat cu succes"

#: ../AppInstall/DialogComplete.py:190
msgid "Do you want to install or remove further software?"
msgstr "Doriți să continuați instalarea sau dezinstalarea de alte programe?"

#: ../AppInstall/DialogComplete.py:195
msgid "Applications have been removed successfully"
msgstr "Aplicațiile au fost dezinstalate cu succes"

#: ../AppInstall/DialogComplete.py:196
msgid "Do you want to install or remove further applications?"
msgstr "Doriți să continuați instalarea sau dezinstalarea de alte aplicații?"

#: ../AppInstall/DialogMultipleApps.py:30
#, python-format
msgid "Remove %s and bundled applications?"
msgstr "Dezinstalați %s împreună cu aplicațiile include?"

#: ../AppInstall/DialogMultipleApps.py:31
#, python-format
msgid ""
"%s is part of a software collection. If you remove %s, you will remove all "
"bundled applications as well."
msgstr ""
"%s face parte dintr-o colecție de programe. În cazul în care dezinstalați %"
"s, veți dezinstala și toate aplicațiile ce fac parte din colecție."

#: ../AppInstall/DialogMultipleApps.py:34
msgid "_Remove All"
msgstr "_Dezinstalează toate"

#: ../AppInstall/DialogMultipleApps.py:36
#, python-format
msgid "Install %s and bundled applications?"
msgstr "Instalați %s împreună cu aplicațiile include?"

#: ../AppInstall/DialogMultipleApps.py:37
#, python-format
msgid ""
"%s is part of a software collection. If you install %s, you will install all "
"bundled applications as well."
msgstr ""
"%s face parte dintr-o colecție de programe. În cazul în care instalați %s, "
"veți instala și toate aplicațiile ce fac parte din colecție."

#: ../AppInstall/DialogMultipleApps.py:40
msgid "_Install All"
msgstr "_Instalează toate"

#: ../AppInstall/DialogProprietary.py:24
#, python-format
msgid "%s is provided by a third party vendor."
msgstr "%s este oferit de o sursă terță."

#: ../AppInstall/DialogProprietary.py:41
msgid ""
"The application comes with the following license terms and conditions. Click "
"on the 'Enable' button to accept them:"
msgstr ""
"Aplicația vine cu următoarele condiții și licențe. Clic pe butonul "
"„Activează” pentru a le accepta:"

#: ../AppInstall/DialogProprietary.py:47
msgid "Accept the license terms and install the software"
msgstr "Acceptă termenii licenței și instalează aplicația"

#: ../AppInstall/Menu.py:128
msgid "Loading cache..."
msgstr "Se deschide lista temporară (cache) ..."

#: ../AppInstall/Menu.py:132
msgid "Collecting application data..."
msgstr "Se colectează informații despre aplicație..."

#: ../AppInstall/Menu.py:372
msgid "Loading applications..."
msgstr "Se încarcă aplicațiile ..."

#. add "All" category
#: ../AppInstall/Menu.py:375
msgid "All"
msgstr "Toate"

#: ../AppInstall/Menu.py:385
#, python-format
msgid "Loading %s..."
msgstr "Se încarcă %s..."

#: ../AppInstall/distros/Debian.py:14 ../AppInstall/distros/Default.py:13
#. %s is the name of the component
#: ../AppInstall/distros/Debian.py:26 ../AppInstall/distros/Default.py:27
#: ../AppInstall/distros/Debian.py:28
msgid "Enable the installation of officially supported Debian software?"
msgstr "Activează instalarea aplicațiilor Debian suportate oficial?"

#. %s is the name of the application
#: ../AppInstall/distros/Debian.py:31
#, fuzzy, python-format
msgid ""
"%s is part of the Debian distribution. Debian provides support and security "
"updates, which will be enabled too."
msgstr ""
"%s face parte din distribuția principală Debian. Debian oferă suport și "
"actualizări de securitate, acestea vor fi deasemenea activate."

#: ../AppInstall/distros/Debian.py:56
#, python-format
msgid "Debian provides support and security updates for %s"
msgstr "Debian oferă suport și actualizări de securitate pentru %s"

#: ../AppInstall/distros/Debian.py:61
#, fuzzy, python-format
msgid "%s is not an official part of Debian."
msgstr "%s nu este suportat în mod oficial cu actualizări de securitate."

#. Fallback
#. %s is the name of the application
#. %s is the name of the application
#. %s is the name of the application
#: ../AppInstall/widgets/AppDescView.py:16
msgid "Description"
msgstr "Descriere"

#: ../AppInstall/widgets/AppDescView.py:88
#, python-format
msgid "%s cannot be installed"
msgstr "Nu s-a putut instala „%s”"

#. warn that this app is not available on this plattform
#: ../AppInstall/widgets/AppDescView.py:97
#, python-format
msgid ""
"%s cannot be installed on your computer type (%s). Either the application "
"requires special hardware features or the vendor decided to not support your "
"computer type."
msgstr ""
"%s nu s-a putut instala pe tipul calculatorui dumneavoastră (%s). Fie "
"această aplicație necesită caracteristici hardware speciale, fie "
"producătorul nu suportă tipul calculatorului dumneavoastră."

#: ../AppInstall/widgets/AppDescView.py:108
#, python-format
msgid ""
"%s is available in the third party software channel '%s'. To install it, "
"please click on the checkbox to activate the software channel."
msgstr ""
"%s este disponibil prin arhiva terță „%s”. Pentru a instala vă rugăm să "
"bifați pentru a activa arhiva."

#: ../AppInstall/widgets/AppDescView.py:118
msgid "This application is bundled with the following applications: "
msgstr "Această aplicație este livrată împreună cu următoarele aplicații: "

#: ../AppInstall/widgets/AppDescView.py:212
#, python-format
msgid ""
"\n"
"Homepage: %s\n"
msgstr ""
"\n"
"Pagina web: %s\n"

#: ../AppInstall/widgets/AppDescView.py:214
#, python-format
msgid "Version: %s (%s)"
msgstr "Versiune: %s (%s)"

#: ../AppInstall/widgets/AppListView.py:55
msgid "Popularity"
msgstr "Popularitate"

#. Application column (icon, name, description)
#: ../AppInstall/widgets/AppListView.py:79
msgid "Application"
msgstr "Aplicații"

#: ../data/trisquel-app-install.ui.h:3
msgid ""
"<big><b>Checking installed and available applications</b></big>"
msgstr ""
