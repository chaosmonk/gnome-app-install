#!/usr/bin/env python3

from setuptools import setup
import os.path
import re

if __name__ == "__main__":

    # look/set what version we have
    changelog = "debian/changelog"
    if os.path.exists(changelog):
        head = open(changelog).readline()
        match = re.compile(".*\((.*)\).*").match(head)
        if match:
            version = match.group(1)
            f = open("AppInstall/Version.py", "w")
            f.write("VERSION=\"%s\"\n" % version)
            f.close()

    GETTEXT_NAME = "trisquel-app-install"

    setup(name='trisquel-app-install',
          version=version,
          packages=['AppInstall',
                    'AppInstall.distros',
                    'AppInstall.backend',
                    'AppInstall.packaging',
                    'AppInstall.widgets'],
          scripts=['trisquel-app-install'],
          data_files=[('share/trisquel-app-install/',
                       ["data/trisquel-app-install.ui"]
                       ),
                      ('sbin',
                       ["update-app-install",
                        'trisquel-app-install-helper']
                       )
                      ],
          test_suite="nose.collector",
          )
